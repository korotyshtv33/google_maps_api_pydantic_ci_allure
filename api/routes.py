from enum import Enum


class Routes(str, Enum): # Enum makes class iterable


    ROUTES = '/maps/api/place/{}/json'
    KEY = '?key=qaclick123'
    KEYID = '&place_id={}'


    OBJECT_GET = ROUTES + KEY + KEYID
    OBJECT_POST =  ROUTES + KEY
    OBJECT_PUT =  ROUTES
    OBJECT_DELETE =  ROUTES + KEY


    def __str__(self) -> str:
        return self.value

