from api import routes


# def get_objects(client, *ids):
#     print(routes.Routes.OBJECTS)
#     return client.get(routes.Routes.OBJECTS, params={'place_id': ids} if ids else None) #


def get_object(client, obj_id):
    # add id inside {} in path(route)
    ROUTE_METHOD = 'get'
    return client.get((routes.Routes.OBJECT_GET).format(ROUTE_METHOD, obj_id))



def post_object(client, **kwargs):
    ROUTE_METHOD = 'add'
    return client.post(routes.Routes.OBJECT_POST.format(ROUTE_METHOD), **kwargs)


def put_object(client,  **kwargs):
    ROUTE_METHOD = 'update'
    return client.put(routes.Routes.OBJECT_PUT.format(ROUTE_METHOD), **kwargs)


def delete_object(client,  **kwargs):
    ROUTE_METHOD = 'delete'

    return client.request(
        method="DELETE",
        url=routes.Routes.OBJECT_DELETE.format(ROUTE_METHOD),
        **kwargs
    )
