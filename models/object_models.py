import uuid
from typing import List, Optional

from pydantic import BaseModel, Field
from pydantic.dataclasses import dataclass


class ObjectCreateOutSchema(BaseModel):
    status: str
    place_id: str
    scope: str
    reference: str
    id: str


class CustomObjCreateOutSchema(BaseModel):
    status: str
    place_id: str
    scope: str
    reference: str
    id: str


class PutResponseSchema(BaseModel):
    msg: str


class Location(BaseModel):
    latitude: float
    longitude: float


@dataclass
class GetResponseSchema(BaseModel):
    location: Location
    accuracy: int
    name: str
    phone_number: str
    address: str
    types: str
    website: str
    language: str
