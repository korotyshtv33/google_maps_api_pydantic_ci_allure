import logging
import os
from http import HTTPStatus

import pytest
from dotenv import load_dotenv

from api.api_client import ApiClient
from api.objects_api import post_object, delete_object, get_object
from assertions.assertion_base import assert_status_code, assert_schema
from models.object_models import CustomObjCreateOutSchema
from utilities.files_utils import read_json_common_request_data
from utilities.logger_utils import logger


def pytest_configure(config):
    # устанавливаем текущую директорию на корень проекта (это позволит прописывать относительные пути к файлам)
    os.chdir(os.path.dirname(os.path.abspath(__file__)))

    # загружаем переменные-параметры из файла /.env
    load_dotenv(dotenv_path=".env")

    # задаем параметры логгера
    path = "logs/"
    os.makedirs(os.path.dirname(path), exist_ok=True)
    file_handler = logging.FileHandler(path + "/info.log", "w")
    file_handler.setLevel(logging.INFO)
    file_handler.setFormatter(logging.Formatter("%(lineno)d: %(asctime)s %(message)s"))

    # создаем кастомный логгер
    custom_logger = logging.getLogger("custom_loger")
    custom_logger.setLevel(logging.INFO)
    custom_logger.addHandler(file_handler)


def pytest_runtest_setup(item):
    logger.info(f"{item.name}:")

@pytest.fixture(scope='class')
def client():
    return ApiClient()

@pytest.fixture(scope='function')
def param_place_id():
    print('create object data')
    client = ApiClient()
    # # записываем объект в базу со всеми заполненными полями
    exp_obj = read_json_common_request_data("valid_post_object")
    response = post_object(client, json=exp_obj)
    # # убеждаемся, что объект успешно записан в базу
    assert_status_code(response, HTTPStatus.OK)
    param_place_id = response.json()['place_id']
    yield param_place_id
    ex_obj = read_json_common_request_data("valid_delete_object")
    ex_obj["place_id"] = param_place_id

    # убеждаемся, что объект удален
    try:
        response = get_object(client, param_place_id)
        assert_status_code(response, HTTPStatus.OK)
    except AssertionError:
        pass

    else:
        response = delete_object(client, json=ex_obj)
        assert_status_code(response, HTTPStatus.OK)


    print('object data deleted')

