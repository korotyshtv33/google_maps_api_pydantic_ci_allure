from http import HTTPStatus

import allure

from api.api_client import ApiClient
from api.objects_api import  delete_object
from assertions.assertion_base import assert_status_code, assert_value_response,  assert_not_exist

from utilities.files_utils import  read_json_common_request_data, read_json_common_response_data


class TestObjectsDelete:
    """
    Тесты /objects
    """
    @allure.feature('Delete object')
    @allure.story('delete exist object')
    def test_delete_exist_object(self, client: ApiClient, param_place_id):
        """
        удаление сущестующего объекта,

        """
        # записываем объект в базу со всеми заполненными полями
        with allure.step('Precondition retrieve exist id'):
            ex_obj = read_json_common_request_data("valid_delete_object")
            ex_obj["place_id"] = param_place_id
        with allure.step("request delete send"):
            response = delete_object(client, json=ex_obj)

        # убеждаемся, что объект удален
        with allure.step('check status code 200'):
            assert_status_code(response, HTTPStatus.OK)
        with allure.step('match body response'):
            exp = read_json_common_response_data("delete_response")
            assert_value_response(response, exp)

    def test_delete_not_exist_object(self, client: ApiClient, request):
        """
        удаление несущестующего объекта,

        """
        # пытаемся удалить несуществующий объект
        del_obj = read_json_common_request_data("invalid_delete_object")
        invalid_place_id = del_obj['place_id']
        del_obj = read_json_common_request_data("valid_delete_object")
        del_obj['place_id'] = invalid_place_id
        response = delete_object(client, json=del_obj)
        # убеждаемся, что сервер дал NOT FOUND ответ
        assert_status_code(response, HTTPStatus.NOT_FOUND)
        assert_not_exist(request, response)
