import time
from http import HTTPStatus

import pytest

from api.api_client import ApiClient
from api.objects_api import get_object
from assertions.assertion_base import assert_status_code, assert_schema, assert_not_found_get
from models.object_models import GetResponseSchema



class TestObjectsGet:
    """
    Тесты /objects
    """

    def test_get_objects_id_param(self, client: ApiClient, param_place_id):
        """
        получение заранее заготовленных объектов из базы с параметром place_id,

        """
        # получаем массив объектов с определенными айдишниками
        response = get_object(client, param_place_id)
        # убеждаемся, что в ответ пришли именно те объекты, id которых мы запросили
        assert_status_code(response, HTTPStatus.OK)
        assert_schema(response, GetResponseSchema)

    def test_get_objects_not_exist_id(self, client: ApiClient, request):
        """
        попытка получить из базы объект с несуществующим id,
        GET /objects
        """
        # пытаемся получить объект, несуществующий в системе
        response = get_object(client, 8523697415)

        # убеждаемся, что в ответ пришел пустой список
        assert_status_code(response, HTTPStatus.NOT_FOUND)
        assert_not_found_get(request, response)
