from http import HTTPStatus

import pytest
from assertions.objects_assertion import should_be_posted_success, should_be_updated_success, should_be_deleted_success, \
    should_be_valid_objects_response

from api.api_client import ApiClient
from api.objects_api import  get_object, post_object, put_object, delete_object
from assertions.assertion_base import assert_status_code, assert_response_body_fields, assert_bad_request, \
    assert_empty_list, assert_schema, assert_not_exist, assert_not_found_put
from models.object_models import PutResponseSchema
from utilities.files_utils import read_json_test_data, read_json_common_request_data


class TestObjectsPut:
    """
    Тесты /objects
    """

    def test_put_object_with_full_body(self, client: ApiClient, param_place_id):
        """
        обновление  поля адреса объекта в базе,

        """
        # изменяем значение place_id
        put_obj = read_json_common_request_data("valid_put_object")
        put_obj['place_id'] = param_place_id

        response = put_object(client, json=put_obj)
        # # убеждаемся, что объект был успешно обновлен
        assert_status_code(response, HTTPStatus.OK)
        assert_schema(response, PutResponseSchema)

    def test_put_object_update_non_exist_obj(self, client: ApiClient, request):
        """
        попытка обновить несуществующий объект,

        """
        # пытаемся обновить несуществующие объект
        put_obj = read_json_common_request_data("invalid_put_object")[1]
        invalid_place_id = put_obj['place_id']
        put_obj = read_json_common_request_data("valid_put_object")
        put_obj['place_id'] = invalid_place_id
        response = put_object(client, json=put_obj)

        # убеждаемся, что сервер дал NOT FOUND ответ
        assert_status_code(response, HTTPStatus.NOT_FOUND)
        assert_not_found_put(request, response)
