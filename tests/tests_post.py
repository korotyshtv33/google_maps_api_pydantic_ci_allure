from http import HTTPStatus

import pytest
from assertions.objects_assertion import should_be_posted_success, should_be_updated_success, should_be_deleted_success, \
    should_be_valid_objects_response

from api.api_client import ApiClient
from api.objects_api import  get_object, post_object, put_object, delete_object
from assertions.assertion_base import assert_status_code, assert_response_body_fields, assert_bad_request, assert_empty_list, assert_schema, assert_not_exist
from models.object_models import  CustomObjCreateOutSchema
from utilities.files_utils import read_json_test_data, read_json_common_request_data


class TestObjectsPost:
    """
    Тесты /objects
    """

    def test_post_object_with_full_body(self, client: ApiClient):
        """
        запись объекта в базу полностью заполненным телом,

        """
        # записываем объект в базу со всеми заполненными полями
        exp_obj = read_json_common_request_data("valid_post_object")
        exp_obj.update()
        response = post_object(client, json=exp_obj)
        # убеждаемся, что объект успешно записан в базу
        assert_status_code(response, HTTPStatus.OK)
        assert_schema(response, CustomObjCreateOutSchema)
        #should_be_posted_success(client, response) !!!!  исправить когда сделаю get



